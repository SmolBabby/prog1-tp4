﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static System.Net.Mime.MediaTypeNames;

namespace TP4
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    /// !!!! NE PAS METTRE DE CODE ICI !!!! 

    public partial class MainWindow : Window
    {

        // #############################
        // #### VARIABLES GLOBALES #####
        // Ces variables sont accessible partout dans le code arrière.

        // La matrice à utiliser pour récupérer les données de votre fichier .CSV
        public static string[,] matrice;

        // Vecteur des jeux
        static Jeu[] jeux;


        // Votre VECTEUR D'OBJETS (sa taille sera déterminée par le nombre de lignes de la matrice
        // TODO
        public MainWindow()
        {
            // NE PAS RETIRER CE CODE
            InitializeComponent();

            // Image placeholder
            AffecterSourceImage("Placeholder.jpg", imgIcone);

            ActivationControles(false);

            // Doit être activer/désactiver en dehors de la fonction
            btnExporter.IsEnabled = false;
        }





        //########################## VOS FONCTIONS ###############################
        //########################################################################
        //C'est à partir d'ici que vous coder votre propres fonctions, nécessaires pour votre TP.

        /// <summary>
        /// Lire le fichier CSV et affecter son contenu à la variable globale "matrice"
        /// </summary>
        /// <param name="cheminAcces"></param>
        static void LireCsvChargerMatrice(string cheminAcces)
        {

            // Ouvrir le fichier CSV
            StreamReader fichierEntree = new StreamReader(cheminAcces);

            fichierEntree.ReadLine();
            // LIRE tout le fichier, INCLUANT la ligne d'entête
            string contenuFichier = fichierEntree.ReadToEnd();
            contenuFichier = contenuFichier.Replace("\r", "");
            string[] lignesFichierTemporaire = contenuFichier.Split('\n');
            string[] lignesFichier;

            // Si la dernière ligne est vide, on l'exclue dans le vecteur de lignes
            if (lignesFichierTemporaire[lignesFichierTemporaire.Length - 1] == "")
            {
                // On dimensionne le vecteur finale de taille -1
                lignesFichier = new string[lignesFichierTemporaire.Length - 1];
            }
            else
            {   // Sinon, on dimensionne le vecteur final de même taille.
                lignesFichier = new string[lignesFichierTemporaire.Length];
            }

            for (int i = 0; i < lignesFichier.Length; i++)
            {
                lignesFichier[i] = lignesFichierTemporaire[i];
            }

            // Nombre de lignes attendues dans la matrice
            int nbLignes = lignesFichier.Length;
            string[] UneLigne = lignesFichier[0].Split(';');
            // Nombre de colonnes attendues dans la matrice
            int nbColonnes = UneLigne.Length;

            // Création de la matrice avec la bonne dimension. Rappel : cette variable
            // est déjà déclarée globalement au dessus de MainWindow()
            matrice = new string[nbLignes, nbColonnes];

            // Affectation de la matrice à partir des données de lignesFichier; 
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                string[] vecteurLigne = lignesFichier[i].Split(';');
                for (int j = 0; j < matrice.GetLength(1); j++)
                {
                    matrice[i, j] = vecteurLigne[j];
                }
            }

            jeux = new Jeu[matrice.GetLength(0)];
            // Pas besoin de retourner la matrice car la variable est disponible globalement.

        }


        /// <summary>
        /// Événement qui va essentiellement lancer la fonction LireCsvChargerMatrice avec le bon chemin d'accès, 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnChargerFichier_Click(object sender, RoutedEventArgs e)
        {
            // 1) Appel de la fonction LireCsvChargerMatrice. Une fois exécutée correctement, la variable "matrice" devrait
            // Contenir toutes les lignes du fichier CSV.
            string cheminAcces = @"C:\data\420-04A-FX\TP4\TP4.csv";
            LireCsvChargerMatrice(cheminAcces);

            // 2) À partir de la matrice, créer vos OBJETS et les AFFECTER au fur et à mesure dans votre VECTEUR D'OBJETS
            // Votre vecteur d'objets devrait être déclarée globalement (au même endroit que la matrice) et il obtiendra 
            // sa taille ici, en fonction du nombre de ligne de la matrice -1 (-1 car il faut exclure la ligne d'entête).

            for (int i = 0; i < matrice.GetLength(0); i++)
            {

                Jeu tempJeu = new Jeu();
                tempJeu.id = Convert.ToInt32(matrice[i, 0]);
                tempJeu.nom = matrice[i, 1];
                tempJeu.categorie = matrice[i, 2];
                tempJeu.prix = Convert.ToDouble(matrice[i, 3]);
                tempJeu.date = Convert.ToDateTime(matrice[i, 4]);
                tempJeu.note = Convert.ToDouble(matrice[i, 5]);

                if (matrice[i, 6].ToLower() == "oui") { tempJeu.achete = true; }
                else { tempJeu.achete = false; }

                tempJeu.img = matrice[i, 7]; //PLACEHOLDER

                jeux[i] = tempJeu;
            }

            MessageBox.Show("Données chargées avec succès.");


            // 3) Une fois votre VECTEUR D'OBJETS créé et affecté, vous pouvez automatique remplir les champs de votre formulaire
            // avec votre 1er objet du vecteur.
            CmbBoxJeux.SelectedIndex = 0;
            RafraichirCmbBoxJeux();

            // Activer les controles
            ActivationControles(true);
        }


        /// <summary>
        /// Fonction du bouton de modification. Enregistre les modifications faite sur 1 jeu dans le vecteur de jeux
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnModifier_Click(object sender, RoutedEventArgs e)
        {
            Jeu nouveauJeu = new Jeu();

            nouveauJeu.id = Convert.ToInt32(TxtID.Text);
            nouveauJeu.nom = txtNom.Text;
            nouveauJeu.categorie = txtCategorie.Text;
            nouveauJeu.note = Convert.ToDouble(txtNote.Text);
            nouveauJeu.prix = Convert.ToDouble(txtPrix.Text);
            nouveauJeu.date = Convert.ToDateTime(dprDate.SelectedDate);
            nouveauJeu.achete = Convert.ToBoolean(chkAchete.IsChecked);
            nouveauJeu.img = jeux[CmbBoxJeux.SelectedIndex].img;

            // Remplacer le jeu dans le vecteur des ejux par la version modifiée
            jeux[CmbBoxJeux.SelectedIndex] = nouveauJeu;

            MessageBox.Show("Modifications enregistrées.");

            // Remettre la liste au premier item pour la sauvegarde
            CmbBoxJeux.SelectedIndex = 0;
            RafraichirCmbBoxJeux();

            // Remettre la liste au premier item pour l'affichage
            CmbBoxJeux.SelectedIndex = 0;

            // Rendre accessible le bouton "Exporter"
            btnExporter.IsEnabled = true;
        }


        /// <summary>
        /// Fonction du bouton d'exportation. Enregistre le vecteur de jeux dans le fichier "NAME"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExporter_Click(object sender, RoutedEventArgs e)
        {
            // Ouvrir le fichier CSV
            string cheminAcces = @"C:\data\420-04A-FX\TP4\TP4_FINAL.csv";
            StreamWriter sw = new StreamWriter(cheminAcces);

            // Écrire l'entête
            sw.WriteLine("ID;NOM;CATÉGORIE;PRIX;DATE;NOTE;ACHETÉ?;IMG");

            // Pour chaque jeu, on créer une nouvelle ligne qui contient toutes les informations et on l'écrit dans le fichier
            for (int i = 0; i < jeux.Length; i++)
            {
                string ligneEcriture = "";

                ligneEcriture += Convert.ToString(jeux[i].id) + ';';
                ligneEcriture += jeux[i].nom + ';';
                ligneEcriture += jeux[i].categorie + ';';
                ligneEcriture += Convert.ToString(jeux[i].prix) + ';';

                // Écriture de la date de sortie dans le bon format pour la sauvegarde (AAAA-MM-JJ)
                ligneEcriture += Convert.ToString(jeux[i].date.Year) + '-' + Convert.ToString(jeux[i].date.Month) + '-' + Convert.ToString(jeux[i].date.Day) + ';';

                // Écriture de la posséssion du jeu (true = oui, false = non)
                if (jeux[i].achete)
                {
                    ligneEcriture += "oui" + ';';
                }
                else
                {
                    ligneEcriture += "non" + ';';
                }

                ligneEcriture += jeux[i].img; // Pas de ';', car c'est le dernier attribut


                // Écrire la ligne
                sw.WriteLine(ligneEcriture);
            }

            // Fermer le fichier
            sw.Close();

            MessageBox.Show("Modifications exportées vers : " + cheminAcces);
        }


        /// <summary>
        /// Fonction appelée quand le jeu sélectionné change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbBoxJeux_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            int indexJeu = CmbBoxJeux.SelectedIndex;
            // L'index ne peut pas être en-dessous de 0
            if (indexJeu < 0) { indexJeu = 0; }

            TxtID.Text = jeux[indexJeu].id.ToString();
            txtNom.Text = jeux[indexJeu].nom;
            txtCategorie.Text = jeux[indexJeu].categorie;
            txtPrix.Text = jeux[indexJeu].prix.ToString();
            dprDate.SelectedDate = Convert.ToDateTime(jeux[indexJeu].date);
            txtNote.Text = jeux[indexJeu].note.ToString();
            chkAchete.IsChecked = jeux[indexJeu].achete;

            // Vérifications
            // Le prix ne peut pas être en-dessous de 0
            if (Convert.ToDouble(txtPrix.Text) < 0) { txtPrix.Text = "0"; }
            // La note doit être entre 0 et 10
            if (Convert.ToDouble(txtNote.Text) < 0) { txtNote.Text = "0"; }
            if (Convert.ToDouble(txtNote.Text) > 10) { txtNote.Text = "10"; }

            AffecterSourceImage(jeux[indexJeu].img, imgIcone);
        }

        /// <summary>
        /// Rafraichie la liste déroulante CmbBoxJeux
        /// </summary>
        private void RafraichirCmbBoxJeux()
        {
            // Ajouter les jeux au ComboBox
            CmbBoxJeux.Items.Clear();
            for (int i = 0; i < jeux.Length; i++)
            {
                CmbBoxJeux.Items.Add(jeux[i].nom);
            }
            CmbBoxJeux.SelectedIndex = 0;
        }


        /// <summary>
        /// Fonction qui affecte la source d'un contrôle Image à partir d'un nom de fichier image. L'image en question DOIT existée dans le projet à la racine de celui-ci.
        /// </summary>
        /// <param name="nomFichierImage">Le nom du fichier image qui se trouve à la racine du projet (ex. erreur.png)</param>
        /// <param name="controleImage">Le nom de variable du Contrôle Image du designer sur lequel on veut changer la source (exemple Img1)</param>
        public void AffecterSourceImage(string nomFichierImage, System.Windows.Controls.Image controleImage)
        {
            string x = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = x + @"..\..\" + @"Images\" + nomFichierImage;

            // controleImage.Source = new BitmapImage(new Uri(path));

            // Cette méthode permet la suppression de l'image après (pour la modifier)
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
            image.UriSource = new Uri(path);
            image.EndInit();
            controleImage.Source = image;

        }


        /// <summary>
        /// Affecte une valeur booléenne à tous les controles pour leur activation
        /// </summary>
        /// <param name="valeur">Valeur booléenne à affecter à tous les controles</param>
        private void ActivationControles(bool valeur)
        {
            // Aucun moyen de faire une boucle sans voler de code

            // AJOUTER LES NOUVEAUX CONTROLES ICI
            txtNom.IsEnabled = valeur;
            txtNote.IsEnabled = valeur;
            txtPrix.IsEnabled = valeur;
            txtCategorie.IsEnabled = valeur;
            dprDate.IsEnabled = valeur;
            btnModifier.IsEnabled = valeur;
            CmbBoxJeux.IsEnabled = valeur;
            chkAchete.IsEnabled = valeur;
            btnModifierImage.IsEnabled = valeur;
            btnRecherche.IsEnabled = valeur;

            // TxtID.IsEnabled = valeur;
        }


        /// <summary>
        /// Fonction pour modifier le fichier d'image que le jeu utilise 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnModifierImage_Click(object sender, RoutedEventArgs e)
        {


            // Dialogue pour ouvrir une image personnalisée
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Filtres pour n'avoir que des images
            openFileDialog.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg";

            // Dossier de base
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            if (openFileDialog.ShowDialog() == true)
            {


                string nomImage = jeux[CmbBoxJeux.SelectedIndex].img;
                string path = System.AppDomain.CurrentDomain.BaseDirectory + @"..\..\" + @"Images\" + nomImage;

                // Supprime l'ancienne image
                File.Delete(path);

                // Créer une image avec le fichier chargé
                BitmapImage image = new BitmapImage(new Uri(openFileDialog.FileName));

                // Sauvegarde la nouvelle image dans le dossier "Images" 
                // CODE DE STACKOVERFLOW MODIFIÉ (https://stackoverflow.com/questions/5963172/how-can-i-save-the-picture-on-image-control-in-wpf)
                var encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image));
                using (FileStream stream = new FileStream(path, FileMode.Create))
                {
                    encoder.Save(stream);

                }

                // Recharge la page pour actualiser l'image
                AffecterSourceImage(jeux[CmbBoxJeux.SelectedIndex].img, imgIcone);

                MessageBox.Show("Modifications enregistrées.");
            }
        }

        /// <summary>
        /// Fonction qui recherche dans le vecteur de jeu pour le nom d'un jeu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRecherche_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < jeux.Length; i++)
            {
                if (txtRecherche.Text.ToLower() == jeux[i].nom.ToLower())
                {
                    MessageBox.Show("Jeu trouvé!");

                    CmbBoxRecherche.Items.Clear();

                    // On remli la ComboBoxRecherche avec les attributs du jeu
                    CmbBoxRecherche.Items.Add("ID: " + jeux[i].id);
                    CmbBoxRecherche.Items.Add("Nom: " + jeux[i].nom);
                    CmbBoxRecherche.Items.Add("Catégorie: " + jeux[i].categorie);
                    CmbBoxRecherche.Items.Add("Prix: " + jeux[i].prix + "$");
                    CmbBoxRecherche.Items.Add("Date de sortie: " + Convert.ToString(jeux[i].date.Year) + '-' + Convert.ToString(jeux[i].date.Month) + '-' + Convert.ToString(jeux[i].date.Day));
                    CmbBoxRecherche.Items.Add("Note : " + jeux[i].note + "/10");
                    CmbBoxRecherche.Items.Add("Acheté: " + jeux[i].achete);
                    CmbBoxRecherche.Items.Add("Fichier d'image: " + jeux[i].img);

                    return;
                }
            }
            MessageBox.Show("Aucun jeu n'a été trouvé.");
        }

        /// <summary>
        /// Fonction qui vide tous les champs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVider_Click(object sender, RoutedEventArgs e)
        {
            // On vide tous les champs
            txtNom.Text = "";
            txtNote.Text = null;
            txtPrix.Text = "";
            txtCategorie.Text = "";
            dprDate.SelectedDate = new DateTime(2000, 1, 1);
            chkAchete.IsChecked = false;
        }

        
    }
}
