﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP4
{
    internal class Jeu
    {
        // Valeur en placeholder
        public int id = 0;                                   // Identifiant
        public string nom = "NAME";                          // Nom du jeu
        public string categorie = "CATEGORY";                // Catégorie
        public double prix = 0.99;                           // Prix ($)
        public DateTime date = new DateTime(2000, 1, 1);     // Date de sortie
        public double note = 10.0;                           // Note (sur 10)
        public bool achete = false;                          // Le jeu a-t-il été acheté par l'utilisateur?
        public string img;                                   // Icone du jeu
    }
}
