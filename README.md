# Introduction à la programmation | 420-04A-FX (TP4)
#### 15%


## OBJECTIFS

Créer un programme en se servant des éléments vus en classe, notamment, mais pas exclusivement :
✓ Tout depuis le début de la session
✓ Plus particulièrement en C# :
o Comme structure de données : les types vus, les vecteurs, les matrices et les structures
o Les façons vues de LIRE et ÉCRIRE dans un fichier texte
o Les boucles et autres structures vues (While, DoWhile, For, If, Switch)
o La portion sur les interfaces graphiques

## Énoncé

Le but de ce travail est de réaliser, en équipe, un programme graphique qui permettra d’effectuer des
opérations courantes de gestion de données (Ex. Modifier, Supprimer, Afficher, Rechercher, etc.) à
partir d’une source texte (un fichier CSV).

Pour ce travail, vous êtes libre de choisir le sujet qui vous convient. Cependant, vous devrez développer
un minimum de fonctionnalités dans votre programme et respecter un certain « cadre », et ce peu
importe votre sujet. **Ces contraintes seront identifiées clairement plus bas dans l’énoncé**.


## Source de données au départ du programme (fichier CSV)

Étant libre de choisir vous-même le sujet de votre programme, vous êtes responsable de créer votre
propre source de données de départ. Cette source devra respecter exactement la même structure que
le fichier de départ de dernier travail (un en-tête, x nombres de lignes, les données séparées par des
points-virgules. Votre source de données doit respecter (au minimum) ces règles :

- **Un en-tête** contenant les noms des propriétés
- À l’exception de l’en-tête, chaque ligne représente un OBJET de votre choix
    o Cet objet doit comprendre **au moins 8 propriétés différentes** , dont l’utilisation au
       minimum des données de types de données _string, int, double et bool._
- La première propriété devra être un identifiant numérique unique nommé « **id** » et la deuxième
    propriété devra être « **NOM** » (qui sera le nom de l’objet)
- **Votre fichier CSV de départ doit comprendre au moins 1 en-tête et 4 objets** (rappel : mais il
    pourrait y en avoir un nombre X, le programme doit pouvoir le gérer de façon dynamique)
- Le nom de votre fichier de départ doit être « C:\data\ 420 - 04A-FX\TP4\ **TP4_NOM.csv** » _où Nom_
    _est votre nom. Si vous êtes en équipe :_ **TP4_NOM1_NOM2.csv**

## Séquence d’exécution du programme

```
1) Le programme se lance, et la fenêtre s’affiche. À ce moment-là, aucune donnée (fichier csv) n’est
encore chargée dans le programme.
2) Un bouton « Charger les données » doit lancer une fonction qui permet la lecture du fichier CSV
et la conservation des données en mémoire dans une seule matrice de strings. Ensuite, vous
devez vous créer une fonction qui va lire cette matrice, créer chaque OBJET et les conserver dans
un vecteur D’OBJETS.
a. Un OBJET est le type que vous désirez, en fonction de ce que vous avez choisi comme sujet.
```

**3) Une fois les OBJETS créés en mémoire à partir des données lues de la matrice** , le programme
utilise ce vecteur d’OBJETS afin d’afficher automatiquement le 1er objet dans votre formulaire
(votre interface graphique)

**4)** Votre interface graphique agit comme « éditeur » d’un objet : c’est-à-dire qu’elle doit permettre
de modifier chacune des propriétés de l’objet, **et ce en se servant de contrôles graphiques
appropriés en fonction des propriétés de votre objet**.
a. Par exemple : une propriété « estMort » d’un objet devrait être représentée sous forme
de 2 choix, comme une liste déroulante Oui/Non ou encore d’une case à cocher. **On ne
veut pas que vous utilisiez juste des TextBox pour modifier vos propriétés**. Les TextBox
devraient être réservés à des chaînes de caractères seulement, et sans connaître les
possibles valeurs à l’avance.


## Règles de programmation à respecter

```
1) Je vous donne le projet de départ, qui contient des commentaires et quelques
variables/fonctions de départs de vous devrez utiliser.
a. La variable « matrice » déclarée globalement
b. Un bouton « ChargerCSV » qui lance l’événement « BtnChargerFichier_Click ».
c. Une fonction « LireCsvChargerMatrice » que vous devez compléter.
2) Vos fonctions doivent être définies dans le même fichier que le code C# de l’interface, afin
d’éviter des problèmes d’accès à l’interface (MainWindow.xaml.cs), en dessous de la fonction
« MainWindow() créée par défaut à la création du projet WPF. Voir celle que j’ai déjà
commencé à coder pour vous (LireCsvChargerMatrice)
3) Les variables globales à votre programme doivent être déclarées juste au-dessus de la fonction
MainWindow()
a. Elles seront alors accessibles partout dans le programme et les fonctions
b. Voir la variable « matrice » que j’ai déjà déclarée pour vous
4) Au lancement du programme, un bouton devra permettre de « charger » les données. Les
données proviennent du fichier CSV, et elles doivent être chargée d’abord dans une matrice de
strings. Toutes les lignes doivent être lues, y compris la première ligne entête. Voir la fonction
LireCsvChargerMatrice que vous devez compléter.
5) Vos OBJETS doivent être créés à partir des données de la matrice, et ajoutés un à un dans 1
Vecteur d’OBJETS
```

## Fonctionnalités à coder

Au minimum, votre interface graphique devra proposer les fonctionnalités suivantes. Vous pouvez en
faire plus à votre discrétion.
On doit clairement voir un formulaire qui permet d’éditer un objet.
1) À l’extérieur du formulaire, un Bouton CHARGER (voir code de départ)
a. Lire fichier CSV et afficher à l’écran le 1re objet dans le formulaire
b. Créer vos OBJETS et les insérer dans votre vecteur d’OBJETS
c. Remplir votre formulaire à partir des propriétés du 1er objet de votre vecteur.
2) Dans le haut du formulaire, une LISTE DÉROULANTE (ComboBox) pour changer d’objet affiché
dans le formulaire. Chaque élément de cette liste contient le champ « Id » de l’objet.
3) Au bas du formulaire, un Bouton MODIFIER
a. Modifier l’objet directement à partir du vecteur D’OBJETS
b. Le champ « id » ne peut pas être modifié.
4) Au bas du formulaire, un Bouton VIDER afin de simplement effacer les champs d’un formulaire
seulement sans supprimer pour autant l’objet en mémoire : on veut seulement mettre les
champs à leur état initial vide.
5) À l’extérieur du formulaire, un Bouton SAVE (CSV)
a. Écrire vos données de vos OBJETS dans le même fichier CSV que celui que vous utilisez
au départ. La structure doit être la même. Vous pouvez « coder dur » la première ligne
d’en-tête.
6) À l’extérieur du formulaire, un Bouton RECHERCHER accompagné des champs appropriés
a. Doit permet de rechercher parmi le vecteur d’OBJETS
b. En utilisant au moins 1 des propriétés de l’OBJET
c. Un message s’affiche indiquant si au moins un OBJET a été trouvé
d. Les données de l’OBJET s’affichent dans un champ LISTBOX où chaque item est une
propriété de l’OBJET
i. Si la recherche trouver plus de 1 objet, on affiche simplement le premier trouvé.
7) **Au moins une fonctionnalité de plus** de votre choix, qui doit être agir sur un ou plusieurs objets
et être expliquée dans votre documentation WORD


## Documentation WORD accompagnant votre travail

Dans un seul document Word appelé « TP 4 _NOM 1 (NOM2 SI EN ÉQUIPE).docx » que vous ajoutez **à
votre dossier de projet** , vous devrez :

- Mettre une page titre
- Écrire le nom de chaque membre de l’équipe. Sous chaque nom, on vous demande de dire
    brièvement sur quelles parties du programme vous avez travaillé.
       o Si vous êtes seul, ça ne s’applique pas.
- Expliquer la structure choisie de votre source de données CSV :
    o Quelle chose ou personne avez-vous décidé de représenter?
    o Quelles sont ses propriétés (identifier clairement chaque colonne du fichier CSV) et
       décrire ce que la colonne contient comme information. Si la caractéristique à une
       certaine contrainte de valeurs possibles, identifier également celles-ci.
    o Identifier également en **rouge les 8 propriétés minimum exigées** dans l’énoncé
- Expliquer la fonctionnalité de plus que vous apporter au programme.

## Documentation de votre code

En plus de commenter code de façon générale tel que vu dans le cours, vous devez utiliser les « /// »
afin de COMMENTER et DOCUMENTER clairement **chaque fonction et structure.
1) Des points peuvent être perdu jusqu’à 10 %** sur cet aspect si vous n’avez pas ou presque pas de
commentaires dans votre programme et que les fonctions/structure ne sont pas documentées
correctement avec les balises générées par le « /// » au-dessus de chaque fonction.


## REMISE

### Date et heure de remise

- Voir LÉA / TRAVAUX pour tous les détails de remise pour ce travail

### Remise de votre travail

- **Une seule personne de l’équipe est responsable de la remise**. Vous devez vous coordonner en
    conséquence.
- Le travail est à remettre au même endroit que l’énoncé, sur LÉA.
- **Votre dossier de projet doit comprendre LE DOCUMENT WORD ET VOTRE SOURCE DE**
    **DONNÉES CSV de départ et être nommé ainsi :**
       o « **TPX_NOM-PRENOM** .zip » lorsque vous êtes seul
       o « **TPX_NOM1** - **PRENOM1** - **NOM2** - **PRENOM2** .zip » lorsque vous êtes plusieurs.

